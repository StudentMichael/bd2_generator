package be.kdg.bigdatagenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigDataGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigDataGeneratorApplication.class, args);
    }

}
