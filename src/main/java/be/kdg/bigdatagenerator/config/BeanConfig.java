package be.kdg.bigdatagenerator.config;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

@Configuration
@ConfigurationProperties(prefix = "bean")
@NoArgsConstructor
public class BeanConfig {
    @Getter @Setter
    private int refreshInSeconds;
}
