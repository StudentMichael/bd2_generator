package be.kdg.bigdatagenerator.scheduler;

import be.kdg.bigdatagenerator.service.ConsumentService;
import be.kdg.bigdatagenerator.service.GeneratorService;
import be.kdg.bigdatagenerator.service.ProducentService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@EnableScheduling
public class Schedulers {
    private final ConsumentService consumentService;
    private final ProducentService producentService;
    private final GeneratorService generatorService;
    
    public Schedulers(ConsumentService consumentService, ProducentService producentService, GeneratorService generatorService) {
        this.consumentService = consumentService;
        this.producentService = producentService;
        this.generatorService = generatorService;
    }

    @Scheduled(fixedDelayString = "${bean.refresh.in.seconds}000")
    public void process() {
        System.out.println("Processing scheduled tasks");
        var timestamp = LocalDateTime.now();
        generatorService.recalculateAverages(timestamp);

        new Thread(() -> consumentService.stuurVerbruikInformatie(timestamp)).start();
        new Thread(() -> producentService.stuurProductieInformatie(timestamp)).start();

    }
}
