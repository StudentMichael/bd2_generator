package be.kdg.bigdatagenerator.service;

import be.kdg.bigdatagenerator.config.BeanConfig;
import be.kdg.bigdatagenerator.model.EanType;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Random;

@Component
public class GeneratorService {
    Random rnd = new Random();
    double mean;
    // TODO: Moet aangepast worden als niet per minuut word gegenereerd
    int divider = 60;
    int localMax;
    BeanConfig config;
    int refreshInSeconds;

    public GeneratorService(BeanConfig config) {
        this.config = config;
        localMax = getLocalMaximum(LocalDateTime.now());
        mean = localMax;
        // TODO: gebruiken voor divider
        refreshInSeconds = config.getRefreshInSeconds();
    }

    // Gemiddeld KWh verandert op basis van het uur
    // localMax geeft het gewenste KWh aan
    // mean zal localMax steeds benaderen maar nooit bereiken.
    // In het begin heeft het hoge velociteit maar vertraagt als het maximum benaderd word.
    public void recalculateAverages(LocalDateTime timestamp){
        localMax = getLocalMaximum(timestamp);
        mean = mean + (localMax - mean) / divider;
    }

    private int getLocalMaximum(LocalDateTime timestamp) {
        // Dag in slots van 4u opgedeeld
        switch ((int)Math.ceil(timestamp.getHour()/4)) {
            // 0-4h
            case 1:
                return 5;
            // 4-8h
            case 2:
                return 20;
            // 8-12h
            case 3:
                return 8;
            // 12-16h
            case 4:
                return 12;
            // 16-20h
            case 5:
                return 25;
            // 20-24h
            case 6:
                return 12;
        }
        System.out.println("Unreachable statement, please report a bug to the Galactic Police Dept.");
        return 0;
    }

    // Afronding op 2 decimalen
    // Gegenereerd op basis van Gaussische functie
    public double calculateKWhBasedOnTimestamp(EanType type, LocalDateTime timestamp) {
        return
            Math.round(
                    (rnd.nextGaussian() + mean)
            *100)/100.00;
    }
}
