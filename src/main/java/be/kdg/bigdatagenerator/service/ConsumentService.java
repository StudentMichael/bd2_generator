package be.kdg.bigdatagenerator.service;

import be.kdg.bigdatagenerator.dao.CsvDao;
import be.kdg.bigdatagenerator.model.Consument;
import be.kdg.bigdatagenerator.model.EanType;
import be.kdg.bigdatagenerator.model.EnergyValue;
import be.kdg.bigdatagenerator.model.Producent;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class ConsumentService {
    List<Consument> beans = new ArrayList<>();
    final CsvDao csvDao;
    final GeneratorService generator;
    EanType eanType = EanType.Consument;

    public ConsumentService(CsvDao csvDao, GeneratorService generator) {
        this.csvDao = csvDao;
        this.generator = generator;
        try {
            var eanDevices = csvDao.retrieveFromCsv(eanType.toString());
            for (var device : eanDevices) {
                beans.add(Consument.builder()
//                        .postcode(device.getPostcode())
//                        .stad(device.getStad())
//                        .latitude(device.getLatitude())
//                        .longitude(device.getLongitude())
                        .ean_id(device.getEan_id())
                        .build());
                }

            System.out.println("Consumenten: " + beans.size());
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stuurVerbruikInformatie(LocalDateTime timestamp) {
        var energyValues = calculeerVerbruik(timestamp);
        csvDao.writeToCsv(energyValues, "consumption");
    }

    public List<EnergyValue> calculeerVerbruik(LocalDateTime timestamp) {
        var verbruikList = new ArrayList<EnergyValue>();

        beans.parallelStream().forEach(e -> verbruikList.add(EnergyValue.builder()
                .ean_id(e.getEan_id())
                .kw(generator.calculateKWhBasedOnTimestamp(eanType, timestamp))
                .timestamp(timestamp)
                .eanType(eanType)
                .build()));

        return verbruikList;
    }

}
