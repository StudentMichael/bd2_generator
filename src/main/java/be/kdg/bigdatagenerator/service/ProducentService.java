package be.kdg.bigdatagenerator.service;

import be.kdg.bigdatagenerator.dao.CsvDao;
import be.kdg.bigdatagenerator.model.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@SuppressWarnings("unchecked")
public class ProducentService {
    List<Producent> beans = new ArrayList<>();
    final CsvDao csvDao;
    final GeneratorService generator;
    EanType eanType = EanType.Producent;

    public ProducentService(CsvDao csvDao, GeneratorService generator) {
        this.csvDao=csvDao;
        this.generator = generator;
        try {
            var eanDevices = csvDao.retrieveFromCsv(eanType.toString());
            for (var device: eanDevices) {
                beans.add(Producent.builder()
//                        .postcode(device.getPostcode())
//                        .stad(device.getStad())
//                        .latitude(device.getLatitude())
//                        .longitude(device.getLongitude())
                        .ean_id(device.getEan_id())
                        .build());
            }
            System.out.println("Producenten: " + beans.size());

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void stuurProductieInformatie(LocalDateTime timestamp) {
        List<EnergyValue> energyValues = calculeerVerbruik(timestamp);
        csvDao.writeToCsv(energyValues, "production");
    }

    public List<EnergyValue> calculeerVerbruik(LocalDateTime timestamp) {
        var verbruikList = new ArrayList<EnergyValue>();

        beans.parallelStream().forEach(e -> verbruikList.add(EnergyValue.builder()
                .ean_id(e.getEan_id())
                .kw(generator.calculateKWhBasedOnTimestamp(eanType, timestamp))
                .timestamp(timestamp)
                .eanType(eanType)
                .build()));

        return verbruikList;
    }
}
