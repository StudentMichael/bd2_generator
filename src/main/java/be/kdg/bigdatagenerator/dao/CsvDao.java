package be.kdg.bigdatagenerator.dao;

import be.kdg.bigdatagenerator.model.EanDevice;
import be.kdg.bigdatagenerator.model.EnergyValue;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.stereotype.Component;

import java.io.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Component
public class CsvDao {
    public CsvDao() {
        init();
    }

    private void init() {
        File directory = new File("output");
        boolean res = directory.mkdir();

        if(res) {
            directory = new File("output/consumption");
            directory.mkdir();
            directory = new File("output/production");
            directory.mkdir();
            System.out.println("Output directories have been created.");
        }
    }

    public List<EanDevice> retrieveFromCsv(String type) throws FileNotFoundException {
        System.out.println("Retrieving data from " + type + ".csv");
        return new CsvToBeanBuilder(new FileReader("src/main/resources/data/" + type +".csv"))
                .withType(EanDevice.class).build().parse();
    }

    public void writeToCsv(List<EnergyValue> list, String dir) {
        var timestamp =  Timestamp.from(Instant.now()).toString();
        var nanos = Timestamp.from(Instant.now()).getNanos();
        System.out.println("Saving to " + dir);
        list.parallelStream().forEach(energyValue -> {
            var fileToWrite = "output/"
                    + dir + "/"
                    + energyValue.getEan_id() + "_"
                    + nanos + ".csv";

           File file = new File(fileToWrite);
            try {
                file.createNewFile();

                Writer writer = new FileWriter(fileToWrite);
                StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
                beanToCsv.write(energyValue);
                writer.close();
            } catch (java.io.IOException ex) {
                System.out.println(ex.getMessage());
            } catch (Exception ex) {
                System.out.println("Unhandled exception! " + ex.getMessage());
            }
        });

        System.out.println("Done saving to " + dir);
    }
}
