package be.kdg.bigdatagenerator.model;

import com.opencsv.bean.CsvBindByName;
import lombok.Builder;
import lombok.Getter;

@Getter
public class Producent extends EanDevice {
    @Builder
    public Producent(int postcode, String stad, double longitude, double latitude, int ean_id) {
        super(postcode, stad, longitude, latitude, ean_id);
    }
}
