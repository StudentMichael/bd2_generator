package be.kdg.bigdatagenerator.model;

import com.opencsv.bean.CsvBindByName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@AllArgsConstructor
@NoArgsConstructor
public class EanDevice {
    @CsvBindByName
    protected int postcode;

    @CsvBindByName
    protected String stad;

    @CsvBindByName
    protected double longitude;

    @CsvBindByName
    protected double latitude;

    @CsvBindByName
    protected int ean_id;

}
