package be.kdg.bigdatagenerator.model;

import lombok.Builder;

public class Consument extends EanDevice{

    @Builder
    public Consument(int postcode, String stad, double longitude, double latitude, int ean_id) {
        super(postcode, stad, longitude, latitude, ean_id);
    }
}
