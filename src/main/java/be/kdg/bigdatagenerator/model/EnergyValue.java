package be.kdg.bigdatagenerator.model;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Builder
public class EnergyValue {
    @Getter
    protected int ean_id;
    protected double kw;
    protected LocalDateTime timestamp;
    protected EanType eanType;
}
