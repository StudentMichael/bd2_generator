# BD2_generator

## Beschrijving
Generator voor KWh consumptie/productie te gebruiken bij Big Data Analytics in Google Cloud

Gegenereerde bestanden worden opgeslagen in de root folder/output/`Consument of Producent`/

## Vereisten
Java 11 JDK

## Opstarten
build.gradle runnen, Gradle project herladen en starten maar!

## Project status
### TODO: 
* Google Cloud integratie
* Acceleratiefunctie aanpassen op basis van rerun tijd via variabele divider (service.GeneratorService.recalculateAverages())
